﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DAL.Entities
{
    public class ProjectTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }

        public int TaskStateId { get; set; }
        public TaskState State { get; set; }

        public int PojectId { get; set; }
        public Project Poject { get; set; }

        public int? PerformerId { get; set; }
        public User Performer { get; set; }
    }
}
