﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class SeedDataMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Canceled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreationDate", "TeamName" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 7, 14, 6, 13, 35, 279, DateTimeKind.Local).AddTicks(915), "odit" },
                    { 2, new DateTime(2019, 7, 13, 18, 7, 44, 535, DateTimeKind.Local).AddTicks(1299), "iusto" },
                    { 3, new DateTime(2019, 7, 13, 23, 14, 32, 510, DateTimeKind.Local).AddTicks(9402), "temporibus" },
                    { 4, new DateTime(2019, 7, 13, 22, 43, 8, 584, DateTimeKind.Local).AddTicks(2482), "ad" },
                    { 5, new DateTime(2019, 7, 14, 11, 21, 30, 956, DateTimeKind.Local).AddTicks(4964), "similique" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegistrationDate", "TeamId" },
                values: new object[,]
                {
                    { 2, new DateTime(2009, 8, 5, 19, 10, 34, 131, DateTimeKind.Local).AddTicks(4660), "Krystel_Stracke@hotmail.com", "Agustina", "Hintz", new DateTime(2019, 5, 28, 17, 21, 24, 158, DateTimeKind.Local).AddTicks(3035), 1 },
                    { 30, new DateTime(2013, 12, 29, 22, 53, 34, 840, DateTimeKind.Local).AddTicks(4025), "Neil.Leannon22@yahoo.com", "Hyman", "Hartmann", new DateTime(2019, 5, 21, 10, 55, 46, 986, DateTimeKind.Local).AddTicks(1563), 3 },
                    { 45, new DateTime(2006, 11, 28, 13, 59, 31, 229, DateTimeKind.Local).AddTicks(8821), "Modesta.Wisozk@yahoo.com", "Makenna", "Koepp", new DateTime(2019, 5, 18, 10, 16, 49, 121, DateTimeKind.Local).AddTicks(2633), 3 },
                    { 49, new DateTime(1986, 6, 18, 21, 36, 48, 757, DateTimeKind.Local).AddTicks(8547), "Alize.Upton@gmail.com", "Xavier", "Wisozk", new DateTime(2019, 5, 15, 2, 26, 13, 950, DateTimeKind.Local).AddTicks(8196), 3 },
                    { 6, new DateTime(1975, 3, 16, 12, 44, 26, 634, DateTimeKind.Local).AddTicks(4593), "Weston15@gmail.com", "Mack", "Klocko", new DateTime(2019, 5, 22, 19, 30, 19, 175, DateTimeKind.Local).AddTicks(7277), 4 },
                    { 7, new DateTime(1996, 7, 13, 19, 22, 12, 179, DateTimeKind.Local).AddTicks(6345), "Lacey95@yahoo.com", "Ethel", "Metz", new DateTime(2019, 5, 6, 2, 17, 16, 370, DateTimeKind.Local).AddTicks(938), 4 },
                    { 9, new DateTime(1971, 7, 5, 6, 5, 55, 330, DateTimeKind.Local).AddTicks(7597), "Johnnie50@yahoo.com", "Tristian", "Dicki", new DateTime(2019, 5, 29, 23, 17, 2, 607, DateTimeKind.Local).AddTicks(8112), 4 },
                    { 10, new DateTime(1996, 4, 17, 12, 52, 24, 24, DateTimeKind.Local).AddTicks(4570), "Adolfo_Nitzsche6@gmail.com", "Jonathon", "Beatty", new DateTime(2019, 7, 6, 22, 24, 23, 248, DateTimeKind.Local).AddTicks(5843), 4 },
                    { 17, new DateTime(2010, 5, 17, 14, 45, 37, 374, DateTimeKind.Local).AddTicks(6452), "Rosella.Jast@yahoo.com", "Cathryn", "Harber", new DateTime(2019, 5, 9, 15, 3, 1, 160, DateTimeKind.Local).AddTicks(535), 4 },
                    { 28, new DateTime(2007, 12, 27, 18, 24, 45, 405, DateTimeKind.Local).AddTicks(576), "Bailey_Langosh79@hotmail.com", "Sammie", "Hoppe", new DateTime(2019, 6, 30, 23, 41, 29, 341, DateTimeKind.Local).AddTicks(8603), 4 },
                    { 29, new DateTime(2019, 4, 28, 22, 47, 28, 845, DateTimeKind.Local).AddTicks(8340), "Emory_Abbott@hotmail.com", "Rebeka", "Ryan", new DateTime(2019, 6, 22, 15, 42, 21, 641, DateTimeKind.Local).AddTicks(1862), 4 },
                    { 31, new DateTime(1991, 3, 12, 12, 18, 1, 147, DateTimeKind.Local).AddTicks(9828), "Raphaelle_Aufderhar@hotmail.com", "Walker", "Lind", new DateTime(2019, 6, 7, 4, 26, 25, 735, DateTimeKind.Local).AddTicks(7253), 4 },
                    { 34, new DateTime(2013, 7, 7, 10, 41, 22, 769, DateTimeKind.Local).AddTicks(580), "Ervin_Konopelski84@gmail.com", "Broderick", "Wolf", new DateTime(2019, 6, 1, 9, 27, 42, 262, DateTimeKind.Local).AddTicks(5572), 4 },
                    { 36, new DateTime(1991, 6, 19, 1, 16, 3, 570, DateTimeKind.Local).AddTicks(7601), "Triston.Rosenbaum55@hotmail.com", "Kenyatta", "Kerluke", new DateTime(2019, 7, 5, 17, 0, 36, 378, DateTimeKind.Local).AddTicks(5794), 4 },
                    { 44, new DateTime(1999, 2, 22, 16, 6, 0, 135, DateTimeKind.Local).AddTicks(8425), "Casandra.Cormier@gmail.com", "Jarrod", "Armstrong", new DateTime(2019, 5, 31, 12, 14, 51, 129, DateTimeKind.Local).AddTicks(1484), 4 },
                    { 46, new DateTime(2002, 5, 26, 21, 37, 30, 40, DateTimeKind.Local).AddTicks(6686), "Dedrick_Upton54@hotmail.com", "Desmond", "Franecki", new DateTime(2019, 6, 24, 23, 33, 19, 687, DateTimeKind.Local).AddTicks(1458), 4 },
                    { 47, new DateTime(2005, 11, 26, 7, 34, 0, 35, DateTimeKind.Local).AddTicks(2248), "Tom.Robel@yahoo.com", "Emiliano", "Berge", new DateTime(2019, 6, 26, 14, 58, 0, 347, DateTimeKind.Local).AddTicks(1469), 4 },
                    { 50, new DateTime(1998, 1, 23, 21, 49, 26, 460, DateTimeKind.Local).AddTicks(5376), "Cali92@hotmail.com", "Adela", "Metz", new DateTime(2019, 4, 27, 18, 36, 18, 19, DateTimeKind.Local).AddTicks(7484), 4 },
                    { 1, new DateTime(1988, 5, 22, 18, 43, 22, 666, DateTimeKind.Local).AddTicks(7884), "Lamar88@gmail.com", "Kenny", "Schmeler", new DateTime(2019, 6, 17, 5, 10, 35, 292, DateTimeKind.Local).AddTicks(1389), 5 },
                    { 4, new DateTime(1992, 9, 30, 22, 33, 39, 64, DateTimeKind.Local).AddTicks(8621), "Austen49@hotmail.com", "Phoebe", "Huels", new DateTime(2019, 6, 18, 7, 55, 26, 398, DateTimeKind.Local).AddTicks(5240), 5 },
                    { 24, new DateTime(1977, 5, 20, 18, 58, 58, 351, DateTimeKind.Local).AddTicks(7744), "Zaria11@gmail.com", "Lexi", "Keeling", new DateTime(2019, 6, 1, 13, 36, 31, 823, DateTimeKind.Local).AddTicks(3498), 5 },
                    { 32, new DateTime(2015, 9, 24, 12, 6, 23, 514, DateTimeKind.Local).AddTicks(9320), "Rossie0@hotmail.com", "Stevie", "O'Reilly", new DateTime(2019, 7, 13, 17, 42, 52, 86, DateTimeKind.Local).AddTicks(8308), 5 },
                    { 27, new DateTime(1994, 2, 7, 12, 43, 52, 963, DateTimeKind.Local).AddTicks(2432), "Reva.Borer@gmail.com", "Samanta", "Thompson", new DateTime(2019, 5, 26, 10, 43, 25, 975, DateTimeKind.Local).AddTicks(3233), 3 },
                    { 20, new DateTime(1987, 1, 7, 1, 9, 53, 266, DateTimeKind.Local).AddTicks(9004), "Dessie_Herman@yahoo.com", "Ramon", "Wilderman", new DateTime(2019, 7, 7, 8, 48, 0, 518, DateTimeKind.Local).AddTicks(3853), 3 },
                    { 19, new DateTime(1983, 7, 1, 0, 50, 56, 205, DateTimeKind.Local).AddTicks(999), "Melody_Weber16@gmail.com", "Delilah", "Koch", new DateTime(2019, 6, 8, 23, 2, 42, 245, DateTimeKind.Local).AddTicks(3107), 3 },
                    { 18, new DateTime(1985, 1, 11, 10, 3, 47, 756, DateTimeKind.Local).AddTicks(2123), "Josiane.Stoltenberg36@hotmail.com", "Imelda", "Daniel", new DateTime(2019, 5, 10, 14, 20, 56, 205, DateTimeKind.Local).AddTicks(2657), 3 },
                    { 3, new DateTime(2006, 8, 5, 5, 12, 25, 611, DateTimeKind.Local).AddTicks(1996), "Desiree_Volkman@hotmail.com", "Rosalyn", "Grimes", new DateTime(2019, 6, 7, 8, 36, 19, 608, DateTimeKind.Local).AddTicks(3771), 1 },
                    { 13, new DateTime(2017, 7, 16, 2, 40, 56, 668, DateTimeKind.Local).AddTicks(1819), "Ellis.Schaefer78@yahoo.com", "Horace", "Heaney", new DateTime(2019, 7, 2, 7, 3, 7, 592, DateTimeKind.Local).AddTicks(4405), 1 },
                    { 14, new DateTime(1985, 6, 2, 11, 7, 0, 199, DateTimeKind.Local).AddTicks(710), "Russell.Feest49@hotmail.com", "Ray", "Rutherford", new DateTime(2019, 6, 18, 22, 28, 2, 230, DateTimeKind.Local).AddTicks(2798), 1 },
                    { 16, new DateTime(2015, 9, 10, 5, 25, 57, 997, DateTimeKind.Local).AddTicks(5091), "Monserrate_Huel@yahoo.com", "Alison", "Strosin", new DateTime(2019, 6, 13, 17, 49, 25, 402, DateTimeKind.Local).AddTicks(8523), 1 },
                    { 21, new DateTime(1983, 6, 21, 1, 37, 57, 411, DateTimeKind.Local).AddTicks(1891), "Muhammad86@yahoo.com", "Kiley", "Bauch", new DateTime(2019, 7, 1, 19, 4, 57, 183, DateTimeKind.Local).AddTicks(8995), 1 },
                    { 23, new DateTime(1987, 11, 3, 16, 52, 51, 374, DateTimeKind.Local).AddTicks(4551), "Deontae_Emmerich62@hotmail.com", "Haskell", "Kiehn", new DateTime(2019, 6, 23, 10, 58, 45, 426, DateTimeKind.Local).AddTicks(4388), 1 },
                    { 25, new DateTime(2013, 2, 25, 16, 48, 53, 592, DateTimeKind.Local).AddTicks(7900), "Maia_Reichel@yahoo.com", "Dangelo", "Nolan", new DateTime(2019, 6, 13, 12, 3, 9, 634, DateTimeKind.Local).AddTicks(7973), 1 },
                    { 35, new DateTime(1978, 5, 8, 16, 39, 42, 808, DateTimeKind.Local).AddTicks(3360), "Cordelia.Swaniawski64@hotmail.com", "Oleta", "Pacocha", new DateTime(2019, 5, 9, 15, 7, 54, 789, DateTimeKind.Local).AddTicks(2865), 1 },
                    { 39, new DateTime(2008, 10, 24, 19, 45, 1, 769, DateTimeKind.Local).AddTicks(4171), "Remington.Gulgowski63@gmail.com", "Marge", "Kutch", new DateTime(2019, 6, 17, 11, 12, 41, 325, DateTimeKind.Local).AddTicks(1579), 1 },
                    { 40, new DateTime(1982, 5, 17, 0, 11, 16, 698, DateTimeKind.Local).AddTicks(9241), "Elisabeth.Prosacco48@gmail.com", "Rosalinda", "Hegmann", new DateTime(2019, 6, 28, 3, 37, 26, 484, DateTimeKind.Local).AddTicks(7031), 1 },
                    { 41, new DateTime(1993, 9, 5, 23, 14, 10, 716, DateTimeKind.Local).AddTicks(3164), "Marilou.Bins@gmail.com", "Meagan", "Corwin", new DateTime(2019, 5, 16, 14, 34, 8, 115, DateTimeKind.Local).AddTicks(4812), 5 },
                    { 42, new DateTime(2000, 7, 31, 8, 46, 24, 758, DateTimeKind.Local).AddTicks(2389), "Rey.Fisher18@hotmail.com", "Fred", "Sauer", new DateTime(2019, 6, 27, 5, 31, 7, 824, DateTimeKind.Local).AddTicks(8455), 1 },
                    { 5, new DateTime(1971, 8, 25, 21, 49, 47, 758, DateTimeKind.Local).AddTicks(4076), "Amie.McGlynn10@hotmail.com", "Gabriella", "Huel", new DateTime(2019, 6, 15, 18, 35, 16, 546, DateTimeKind.Local).AddTicks(4321), 2 },
                    { 8, new DateTime(2004, 11, 8, 18, 51, 21, 363, DateTimeKind.Local).AddTicks(2146), "Armando.Hermann91@yahoo.com", "Quinton", "Runolfsdottir", new DateTime(2019, 6, 8, 13, 13, 45, 870, DateTimeKind.Local).AddTicks(4580), 2 },
                    { 15, new DateTime(2000, 10, 14, 19, 58, 11, 271, DateTimeKind.Local).AddTicks(2498), "Roger18@gmail.com", "Catherine", "Toy", new DateTime(2019, 6, 7, 9, 29, 1, 877, DateTimeKind.Local).AddTicks(3157), 2 },
                    { 22, new DateTime(1985, 8, 11, 19, 33, 8, 724, DateTimeKind.Local).AddTicks(6074), "Napoleon.Nienow@gmail.com", "Aisha", "Rippin", new DateTime(2019, 5, 11, 16, 54, 3, 238, DateTimeKind.Local).AddTicks(1373), 2 },
                    { 26, new DateTime(2003, 11, 1, 18, 1, 56, 875, DateTimeKind.Local).AddTicks(3373), "Ricardo.Gusikowski63@gmail.com", "Gaetano", "Hodkiewicz", new DateTime(2019, 7, 12, 9, 32, 10, 804, DateTimeKind.Local).AddTicks(1697), 2 },
                    { 33, new DateTime(1999, 8, 19, 19, 17, 32, 651, DateTimeKind.Local).AddTicks(6490), "Rowland_Bailey74@yahoo.com", "Marcel", "Schaden", new DateTime(2019, 7, 14, 10, 59, 29, 197, DateTimeKind.Local).AddTicks(1494), 2 },
                    { 37, new DateTime(1999, 9, 25, 8, 4, 33, 72, DateTimeKind.Local).AddTicks(7268), "Stevie_Hilpert64@gmail.com", "Wilton", "Spencer", new DateTime(2019, 6, 8, 4, 34, 26, 520, DateTimeKind.Local).AddTicks(184), 2 },
                    { 38, new DateTime(1972, 12, 24, 1, 2, 7, 23, DateTimeKind.Local).AddTicks(7303), "Darrel50@yahoo.com", "Josh", "Daniel", new DateTime(2019, 7, 6, 19, 10, 7, 331, DateTimeKind.Local).AddTicks(4765), 2 },
                    { 11, new DateTime(1998, 4, 24, 11, 2, 40, 853, DateTimeKind.Local).AddTicks(3373), "Antoinette.Becker98@yahoo.com", "Edgar", "Skiles", new DateTime(2019, 4, 23, 3, 12, 22, 48, DateTimeKind.Local).AddTicks(270), 3 },
                    { 12, new DateTime(2012, 2, 3, 1, 1, 17, 11, DateTimeKind.Local).AddTicks(7980), "Myrtis63@gmail.com", "Davion", "Smith", new DateTime(2019, 6, 29, 8, 27, 18, 58, DateTimeKind.Local).AddTicks(8126), 3 },
                    { 43, new DateTime(2005, 4, 27, 20, 34, 33, 914, DateTimeKind.Local).AddTicks(488), "Mikel.Hintz91@hotmail.com", "Vincent", "Herzog", new DateTime(2019, 6, 2, 0, 52, 44, 558, DateTimeKind.Local).AddTicks(7416), 1 },
                    { 48, new DateTime(2010, 3, 27, 6, 25, 23, 820, DateTimeKind.Local).AddTicks(3524), "Carley_Ullrich@hotmail.com", "Glen", "Rice", new DateTime(2019, 6, 29, 7, 5, 47, 692, DateTimeKind.Local).AddTicks(2607), 5 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreationDate", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 2, new DateTime(2017, 11, 29, 16, 27, 56, 14, DateTimeKind.Local).AddTicks(9321), new DateTime(2020, 6, 5, 18, 9, 37, 675, DateTimeKind.Local).AddTicks(1161), @"Rerum aut incidunt voluptatem quaerat temporibus cumque quia.
                Adipisci velit nam velit.
                Rerum iusto modi nulla cupiditate.", "reprehenderit", 1 },
                    { 2, 5, new DateTime(2018, 1, 5, 14, 59, 31, 978, DateTimeKind.Local).AddTicks(151), new DateTime(2020, 5, 26, 6, 3, 27, 168, DateTimeKind.Local).AddTicks(4394), @"Doloribus ut at error illo.
                Quae facilis nisi vero cupiditate maxime soluta.
                Doloremque unde maiores eveniet facere quaerat animi ut eum.", "sunt", 2 },
                    { 3, 11, new DateTime(2015, 3, 26, 22, 7, 20, 763, DateTimeKind.Local).AddTicks(2459), new DateTime(2021, 3, 13, 14, 32, 25, 87, DateTimeKind.Local).AddTicks(9925), @"Vel distinctio assumenda saepe et et voluptatem animi qui saepe.
                Voluptas maiores dicta voluptates deleniti dolorem soluta.
                Voluptatem est in illum.
                Voluptas quisquam sint sunt illo sint et et quod vel.
                Et aut repellendus.
                Incidunt tempora iure.", "dolor", 3 },
                    { 4, 6, new DateTime(2018, 4, 28, 7, 49, 17, 182, DateTimeKind.Local).AddTicks(2201), new DateTime(2019, 7, 30, 2, 22, 59, 306, DateTimeKind.Local).AddTicks(6694), @"Doloremque aut sit eaque rerum.
                Velit voluptas est magni beatae.
                Placeat perspiciatis tempora labore facere.", "veniam", 4 },
                    { 5, 1, new DateTime(2016, 11, 18, 13, 56, 37, 599, DateTimeKind.Local).AddTicks(171), new DateTime(2021, 4, 19, 6, 56, 12, 287, DateTimeKind.Local).AddTicks(6302), @"Voluptates aperiam ullam facilis a ea recusandae iusto.
                Velit accusantium earum totam.
                Aspernatur aspernatur harum accusantium sed expedita delectus earum deleniti.", "quas", 5 }
                });

            migrationBuilder.InsertData(
                table: "ProjectTasks",
                columns: new[] { "Id", "DateOfCreation", "Description", "FinishedAt", "Name", "PerformerId", "PojectId", "TaskStateId" },
                values: new object[,]
                {
                    { 2, new DateTime(2019, 6, 9, 3, 22, 10, 373, DateTimeKind.Local).AddTicks(9317), @"Blanditiis voluptatem accusamus non voluptas dolore pariatur a debitis.
                At ipsam modi accusamus.", new DateTime(2020, 11, 2, 20, 11, 44, 422, DateTimeKind.Local).AddTicks(5132), "accusamus", 2, 1, 4 },
                    { 30, new DateTime(2017, 4, 28, 6, 57, 11, 695, DateTimeKind.Local).AddTicks(7870), @"Aliquam quo et repellat odio eligendi.
                Molestiae aliquid voluptatum beatae.
                Et odit et vero aperiam vel doloremque dolorum.
                Ipsum possimus soluta quia fugiat fuga eligendi esse.
                Tenetur et eum saepe sit qui rem mollitia deleniti.
                Sed omnis et dignissimos animi iusto harum ut.", new DateTime(2019, 10, 23, 6, 50, 38, 986, DateTimeKind.Local).AddTicks(7129), "laudantium", 30, 3, 1 },
                    { 45, new DateTime(2019, 2, 22, 4, 0, 0, 920, DateTimeKind.Local).AddTicks(8935), @"Dolor reiciendis iure similique rem.
                Omnis in omnis in et maiores aut unde expedita quia.
                Sit repellat asperiores quia id rerum.
                Et adipisci eveniet qui ipsum quam.
                Esse facilis molestiae excepturi in unde iure et.", new DateTime(2020, 7, 9, 5, 45, 52, 880, DateTimeKind.Local).AddTicks(2884), "non", 45, 3, 2 },
                    { 49, new DateTime(2017, 3, 19, 20, 47, 16, 5, DateTimeKind.Local).AddTicks(9180), @"Dolores esse ipsa aperiam.
                Ullam aut qui ratione sed voluptas ea est at.
                Saepe quia qui corrupti quia sit recusandae iure.
                Recusandae totam alias dolorem.", new DateTime(2020, 2, 7, 9, 10, 21, 911, DateTimeKind.Local).AddTicks(2771), "odit", 49, 3, 4 },
                    { 6, new DateTime(2014, 10, 13, 0, 56, 58, 207, DateTimeKind.Local).AddTicks(2337), @"Perferendis nesciunt in dolore.
                Delectus mollitia saepe qui.
                Enim autem est voluptatem.
                Eligendi ad qui aliquid.", new DateTime(2019, 11, 6, 12, 7, 25, 15, DateTimeKind.Local).AddTicks(7357), "consequatur", 6, 4, 1 },
                    { 7, new DateTime(2017, 6, 23, 15, 33, 22, 672, DateTimeKind.Local).AddTicks(8231), @"Laborum velit voluptatibus vel natus et et aut nihil veniam.
                Et quos qui aspernatur iusto illum voluptate quibusdam odit dolor.
                Sed nostrum eveniet expedita eligendi.
                Et et quam ullam nihil voluptatibus.
                Optio placeat velit facere dolorem aut nesciunt aut velit nemo.", new DateTime(2021, 1, 31, 0, 0, 27, 975, DateTimeKind.Local).AddTicks(4018), "eos", 7, 4, 4 },
                    { 9, new DateTime(2017, 3, 16, 6, 46, 43, 875, DateTimeKind.Local).AddTicks(9177), @"Esse ex repellat nostrum fugiat nemo explicabo occaecati.
                In nihil provident nihil debitis odit et ullam.
                Aut adipisci occaecati nobis est nobis.
                Dolorum et et in earum.", new DateTime(2020, 10, 9, 3, 45, 26, 567, DateTimeKind.Local).AddTicks(4212), "et", 9, 4, 1 },
                    { 10, new DateTime(2018, 6, 25, 2, 12, 24, 897, DateTimeKind.Local).AddTicks(2855), @"Porro id in et explicabo quia.
                Molestias ex fuga.
                Culpa facere harum consequuntur aliquid sed minus harum.", new DateTime(2020, 2, 11, 20, 54, 58, 472, DateTimeKind.Local).AddTicks(3224), "ipsam", 10, 4, 2 },
                    { 17, new DateTime(2017, 6, 16, 5, 24, 33, 736, DateTimeKind.Local).AddTicks(8735), @"Placeat numquam ut voluptatibus reiciendis dolor aut sit molestias.
                Voluptas et laborum voluptatem vero accusamus.", new DateTime(2020, 5, 8, 21, 54, 33, 466, DateTimeKind.Local).AddTicks(6562), "veritatis", 17, 4, 1 },
                    { 28, new DateTime(2018, 3, 6, 13, 54, 0, 810, DateTimeKind.Local).AddTicks(6703), @"Impedit et alias veniam praesentium voluptas repellat mollitia quae.
                Quisquam aut nulla itaque ipsam.
                Odio aliquid est provident quo doloremque quas distinctio sed consectetur.
                Beatae numquam velit ut hic voluptas voluptate sint.
                Suscipit occaecati sapiente aliquam nulla minus fugit.
                Et a perspiciatis voluptas similique et minus quam aut.", new DateTime(2020, 3, 18, 13, 53, 24, 551, DateTimeKind.Local).AddTicks(7652), "voluptatem", 28, 4, 3 },
                    { 29, new DateTime(2018, 1, 5, 22, 8, 58, 645, DateTimeKind.Local).AddTicks(3933), @"Et eum quidem.
                Assumenda nisi iusto quam quo deserunt sint iure.
                Nobis reiciendis sint cum neque dolore.", new DateTime(2020, 11, 22, 22, 33, 9, 549, DateTimeKind.Local).AddTicks(1325), "est", 29, 4, 1 },
                    { 31, new DateTime(2016, 9, 10, 12, 39, 34, 585, DateTimeKind.Local).AddTicks(4468), @"Beatae natus qui.
                Et dolores molestias omnis occaecati qui ut.
                Molestiae quia eum iusto suscipit sit esse accusamus.
                Ipsam vitae quod sed vitae incidunt voluptas.
                Dicta sed molestias beatae.
                Quas repudiandae est.", new DateTime(2019, 8, 12, 5, 27, 13, 155, DateTimeKind.Local).AddTicks(1919), "accusamus", 31, 4, 2 },
                    { 34, new DateTime(2019, 4, 18, 21, 2, 22, 645, DateTimeKind.Local).AddTicks(8618), @"Unde officiis fugit sint dolor.
                Dolor debitis eos repellendus.", new DateTime(2020, 7, 2, 23, 55, 44, 308, DateTimeKind.Local).AddTicks(3382), "sint", 34, 4, 2 },
                    { 36, new DateTime(2017, 12, 18, 13, 36, 14, 43, DateTimeKind.Local).AddTicks(3419), @"Qui ut magni ut quaerat mollitia laborum explicabo ea placeat.
                Et omnis atque quasi aut earum eius.
                Totam at consequatur dicta voluptas esse.", new DateTime(2020, 4, 18, 13, 56, 27, 74, DateTimeKind.Local).AddTicks(7040), "perspiciatis", 36, 4, 2 },
                    { 44, new DateTime(2017, 1, 18, 17, 0, 43, 187, DateTimeKind.Local).AddTicks(5109), @"Doloremque et facere corporis harum debitis sed autem minus.
                Eligendi eum dolor est vel.
                A sunt ducimus in tempore nihil qui ut et odio.", new DateTime(2020, 2, 27, 22, 38, 37, 978, DateTimeKind.Local).AddTicks(2941), "voluptatibus", 44, 4, 2 },
                    { 46, new DateTime(2017, 3, 15, 8, 13, 6, 260, DateTimeKind.Local).AddTicks(2068), @"Et aut ullam saepe exercitationem.
                Maxime architecto sunt sunt et.
                Minima voluptatem enim.
                Magni tempore est laboriosam quia aperiam.
                Est aperiam quo rem ea.
                Minus natus veniam.", new DateTime(2021, 4, 13, 2, 5, 52, 145, DateTimeKind.Local).AddTicks(1111), "nobis", 46, 4, 1 },
                    { 47, new DateTime(2018, 7, 27, 3, 55, 27, 541, DateTimeKind.Local).AddTicks(110), @"Eveniet at itaque enim.
                Voluptatem suscipit quas voluptas eligendi quod expedita omnis.
                Neque qui facere cupiditate vero culpa.
                Iure repellat voluptas provident dolorum eum nihil iste et.", new DateTime(2020, 6, 2, 17, 17, 19, 679, DateTimeKind.Local).AddTicks(7900), "provident", 47, 4, 3 },
                    { 50, new DateTime(2018, 12, 22, 15, 35, 2, 454, DateTimeKind.Local).AddTicks(5659), @"Est quia vero assumenda rerum tenetur.
                Est corrupti voluptatem dolore et voluptatem magni.
                Sit praesentium quis aut sed accusantium vero aspernatur quis.
                Rerum nostrum consequatur quod dolorem.
                Vel repellat eos sed alias officiis porro quis incidunt.
                Quod optio dolor eius possimus sit.", new DateTime(2020, 2, 3, 5, 54, 13, 323, DateTimeKind.Local).AddTicks(7995), "aut", 50, 4, 1 },
                    { 1, new DateTime(2016, 1, 7, 13, 24, 2, 167, DateTimeKind.Local).AddTicks(969), @"Sit cupiditate deserunt perspiciatis consequuntur.
                Reiciendis assumenda aut esse velit facilis architecto.
                Sint veniam assumenda molestiae non molestias ea.
                Corporis est cum.
                Iste blanditiis fugit laboriosam deleniti consequuntur possimus tenetur et quas.", new DateTime(2019, 10, 10, 1, 35, 4, 54, DateTimeKind.Local).AddTicks(7609), "ut", 1, 5, 4 },
                    { 4, new DateTime(2018, 1, 25, 18, 3, 16, 428, DateTimeKind.Local).AddTicks(304), @"Minus dignissimos repellendus.
                Odio nihil animi sint veritatis.
                In repellendus illo esse illum corporis.", new DateTime(2021, 2, 10, 20, 43, 32, 137, DateTimeKind.Local).AddTicks(7386), "sit", 4, 5, 2 },
                    { 24, new DateTime(2017, 12, 18, 22, 34, 2, 459, DateTimeKind.Local).AddTicks(6989), @"Labore ea et fugit eaque repudiandae repellat.
                Distinctio ut nulla perferendis dolor non delectus deserunt.
                Qui libero velit sit consequatur ut illum est voluptate.", new DateTime(2020, 9, 23, 8, 40, 57, 66, DateTimeKind.Local).AddTicks(2657), "ipsam", 24, 5, 2 },
                    { 32, new DateTime(2017, 8, 1, 11, 14, 39, 286, DateTimeKind.Local).AddTicks(1124), @"Voluptates et qui ut fugit provident omnis.
                Rerum ut voluptas deleniti beatae voluptate voluptatibus voluptatem maxime nemo.", new DateTime(2021, 3, 9, 14, 27, 46, 919, DateTimeKind.Local).AddTicks(7476), "et", 32, 5, 4 },
                    { 27, new DateTime(2016, 9, 27, 14, 39, 44, 741, DateTimeKind.Local).AddTicks(9522), @"Totam laboriosam et nihil.
                Voluptas sit nesciunt.
                Eius et iure.", new DateTime(2020, 4, 21, 23, 44, 24, 916, DateTimeKind.Local).AddTicks(9926), "voluptatem", 27, 3, 3 },
                    { 20, new DateTime(2018, 3, 22, 15, 23, 3, 490, DateTimeKind.Local).AddTicks(7083), @"Expedita minus atque vitae nisi.
                Repudiandae quasi et dolores aut non inventore eius quibusdam cumque.
                Dolorum consequatur labore illum ipsum.
                Et et est.
                Dolor aperiam id non est explicabo doloribus.", new DateTime(2020, 6, 15, 16, 2, 5, 160, DateTimeKind.Local).AddTicks(1252), "pariatur", 20, 3, 2 },
                    { 19, new DateTime(2016, 11, 14, 4, 56, 26, 924, DateTimeKind.Local).AddTicks(8903), @"Laboriosam molestias tenetur qui quisquam et eius deserunt voluptatibus doloremque.
                Dolor saepe eum.", new DateTime(2021, 5, 6, 5, 9, 52, 377, DateTimeKind.Local).AddTicks(8151), "dolore", 19, 3, 4 },
                    { 18, new DateTime(2017, 9, 27, 14, 16, 54, 488, DateTimeKind.Local).AddTicks(952), @"Velit dicta qui est facere.
                Et nihil accusantium eum quibusdam sed dolores culpa possimus.", new DateTime(2022, 5, 16, 23, 34, 32, 903, DateTimeKind.Local).AddTicks(1312), "architecto", 18, 3, 4 },
                    { 3, new DateTime(2018, 3, 28, 6, 57, 35, 553, DateTimeKind.Local).AddTicks(7510), @"Rerum sed soluta.
                Recusandae nulla modi.
                Iure sed officiis sint ut ex.
                Fuga consequuntur et.
                Occaecati magnam qui aspernatur omnis minima.
                Illo perspiciatis dignissimos mollitia numquam animi.", new DateTime(2020, 4, 10, 11, 32, 31, 546, DateTimeKind.Local).AddTicks(5216), "at", 3, 1, 3 },
                    { 13, new DateTime(2015, 12, 23, 19, 19, 5, 847, DateTimeKind.Local).AddTicks(2272), @"Architecto illo numquam eum molestiae aut quo.
                Asperiores incidunt illum est qui assumenda ea nulla sint.
                Eos dolor cum non voluptatem est ut consectetur.
                Modi qui fuga sequi.", new DateTime(2019, 10, 24, 2, 55, 40, 57, DateTimeKind.Local).AddTicks(8565), "dicta", 13, 1, 2 },
                    { 14, new DateTime(2015, 3, 1, 7, 38, 43, 12, DateTimeKind.Local).AddTicks(1610), @"Nobis blanditiis esse et quod temporibus.
                Molestiae quibusdam explicabo voluptatem eum ut ducimus cum.
                Et temporibus cupiditate error id molestiae et quo consequatur architecto.", new DateTime(2019, 11, 30, 17, 58, 31, 721, DateTimeKind.Local).AddTicks(3087), "qui", 14, 1, 1 },
                    { 16, new DateTime(2017, 1, 22, 13, 16, 35, 682, DateTimeKind.Local).AddTicks(1784), @"Et delectus aut ut quos.
                Sunt rerum sunt in quae fuga rerum ipsa.
                Unde esse atque natus id at fugiat eligendi.
                Ex neque occaecati corrupti.", new DateTime(2020, 2, 5, 4, 20, 55, 305, DateTimeKind.Local).AddTicks(6605), "quos", 16, 1, 3 },
                    { 21, new DateTime(2017, 8, 10, 9, 45, 34, 981, DateTimeKind.Local).AddTicks(1197), @"Temporibus cupiditate libero soluta voluptas doloremque dolor sapiente repellat quis.
                Perspiciatis provident atque dolorum nobis numquam et.
                Dolores eaque quidem error nihil.
                Quasi consequatur in.
                Nihil nesciunt occaecati.", new DateTime(2019, 10, 17, 20, 30, 0, 684, DateTimeKind.Local).AddTicks(6306), "natus", 21, 1, 2 },
                    { 23, new DateTime(2016, 3, 19, 21, 18, 21, 943, DateTimeKind.Local).AddTicks(1642), @"Doloremque iste doloremque.
                Minus eos ut aperiam et.
                Similique laborum repellat aut architecto.
                Ut unde sit aperiam ut.
                Rerum dolorum odit et temporibus voluptatem enim sint.", new DateTime(2020, 2, 9, 22, 26, 15, 627, DateTimeKind.Local).AddTicks(6492), "perferendis", 23, 1, 4 },
                    { 25, new DateTime(2015, 12, 8, 23, 10, 29, 123, DateTimeKind.Local).AddTicks(5181), @"Enim laborum consequuntur sed deserunt ut dolorum laudantium voluptatem.
                Nemo exercitationem id corrupti mollitia labore voluptate.
                Maxime voluptatibus laboriosam molestiae.
                Sunt reprehenderit harum.
                Doloribus vero recusandae fugiat amet magni tenetur.
                Ex laudantium maxime aut.", new DateTime(2021, 2, 10, 5, 37, 25, 149, DateTimeKind.Local).AddTicks(9687), "cumque", 25, 1, 3 },
                    { 35, new DateTime(2017, 5, 23, 14, 18, 49, 62, DateTimeKind.Local).AddTicks(8553), @"Aut eligendi et eligendi veritatis.
                Tempore voluptas cumque.", new DateTime(2021, 2, 20, 7, 41, 5, 509, DateTimeKind.Local).AddTicks(3212), "cum", 35, 1, 1 },
                    { 39, new DateTime(2019, 2, 19, 3, 25, 11, 874, DateTimeKind.Local).AddTicks(8855), @"Aut incidunt minima aut voluptas vel.
                Enim voluptates ut blanditiis aut odit in.", new DateTime(2019, 12, 28, 1, 44, 0, 647, DateTimeKind.Local).AddTicks(4644), "voluptates", 39, 1, 1 },
                    { 40, new DateTime(2017, 1, 12, 7, 15, 53, 966, DateTimeKind.Local).AddTicks(1498), @"Voluptatem eius eum reiciendis.
                Laboriosam autem quaerat nulla est reiciendis nesciunt quo.", new DateTime(2020, 8, 4, 12, 3, 10, 480, DateTimeKind.Local).AddTicks(9954), "maiores", 40, 1, 4 },
                    { 41, new DateTime(2016, 4, 8, 6, 0, 58, 410, DateTimeKind.Local).AddTicks(7844), @"Nulla voluptas enim.
                Distinctio repellat dolorem est ratione reiciendis dicta quam dolorum.
                Veritatis ad aliquid impedit.", new DateTime(2021, 2, 24, 5, 0, 4, 259, DateTimeKind.Local).AddTicks(1296), "modi", 41, 5, 4 },
                    { 42, new DateTime(2016, 1, 3, 1, 43, 21, 365, DateTimeKind.Local).AddTicks(2985), @"Et nisi blanditiis eveniet optio ipsa quo qui.
                Necessitatibus iste dolores aut.
                Quia laborum optio tempore saepe.
                Explicabo dolor rem aliquid explicabo qui tempore molestiae.
                Quam occaecati minima eveniet aut beatae.
                Consectetur magnam minima ipsa impedit voluptatum accusantium ipsum porro.", new DateTime(2019, 8, 29, 18, 57, 4, 535, DateTimeKind.Local).AddTicks(4093), "numquam", 42, 1, 4 },
                    { 5, new DateTime(2018, 8, 9, 20, 34, 52, 766, DateTimeKind.Local).AddTicks(3339), @"Quia ad nam aut et nisi vel vel omnis.
                Repellendus consequatur et dolor fugit in.
                Ut atque voluptatem velit molestiae perspiciatis odio similique magni minus.
                Culpa eos numquam.
                Blanditiis asperiores asperiores.
                Ipsa quisquam deserunt porro.", new DateTime(2020, 10, 14, 20, 32, 45, 53, DateTimeKind.Local).AddTicks(7706), "ipsa", 5, 2, 3 },
                    { 8, new DateTime(2018, 5, 17, 7, 16, 26, 221, DateTimeKind.Local).AddTicks(8089), @"Nam velit aliquam distinctio.
                Repudiandae ut recusandae illo eaque quibusdam et libero.
                Molestias velit in nobis ea expedita.
                Repellat dolorum unde consequuntur voluptatibus eaque quibusdam veniam.
                Unde repudiandae libero ratione omnis repellendus qui eaque veritatis.", new DateTime(2019, 8, 27, 22, 1, 27, 387, DateTimeKind.Local).AddTicks(1604), "et", 8, 2, 3 },
                    { 15, new DateTime(2017, 6, 10, 16, 23, 20, 660, DateTimeKind.Local).AddTicks(926), @"Quia quia quaerat quis occaecati.
                Animi omnis iure consequuntur doloribus autem amet quidem dolor eum.
                Corrupti rerum eum est.
                Quo cumque omnis libero veritatis voluptates iste aperiam delectus.", new DateTime(2019, 11, 20, 16, 25, 57, 717, DateTimeKind.Local).AddTicks(8215), "rem", 15, 2, 3 },
                    { 22, new DateTime(2018, 1, 21, 21, 23, 11, 538, DateTimeKind.Local).AddTicks(5969), @"Repellat quasi id facilis sunt unde blanditiis.
                Id ut nihil consequatur ut.
                Officiis voluptatem a dolore error quaerat distinctio.", new DateTime(2019, 8, 11, 11, 41, 44, 102, DateTimeKind.Local).AddTicks(7474), "quia", 22, 2, 1 },
                    { 26, new DateTime(2016, 8, 16, 11, 44, 50, 386, DateTimeKind.Local).AddTicks(2878), @"Eum velit unde quia modi voluptates ut corrupti.
                Omnis eaque est sapiente quis et id sit quisquam quia.
                Repellendus ut labore omnis.
                Quis quae consectetur dolorum aut enim culpa sapiente.", new DateTime(2020, 2, 22, 18, 52, 30, 507, DateTimeKind.Local).AddTicks(1469), "sapiente", 26, 2, 3 },
                    { 33, new DateTime(2019, 4, 29, 16, 58, 17, 168, DateTimeKind.Local).AddTicks(1632), @"Laudantium cumque nihil et laudantium reprehenderit non quia.
                Laudantium dicta dolores magni iste.
                Id magni iste quod iusto eligendi laudantium est dignissimos.
                Est corrupti ut qui voluptatem temporibus quia.", new DateTime(2019, 9, 25, 22, 42, 55, 9, DateTimeKind.Local).AddTicks(5872), "quibusdam", 33, 2, 4 },
                    { 37, new DateTime(2019, 3, 21, 13, 45, 21, 595, DateTimeKind.Local).AddTicks(6575), @"Voluptatem ratione et et asperiores pariatur aspernatur nostrum.
                Nihil minima voluptatem occaecati earum consectetur esse.
                Laudantium qui aspernatur ut doloremque aut quaerat quis.
                Porro explicabo eius libero reiciendis aperiam minus sed est.
                Esse sunt rerum consequatur veniam.", new DateTime(2021, 8, 9, 12, 31, 2, 637, DateTimeKind.Local).AddTicks(6252), "quia", 37, 2, 3 },
                    { 38, new DateTime(2019, 4, 24, 10, 0, 53, 716, DateTimeKind.Local).AddTicks(8921), @"Aut quisquam sint consequuntur est.
                Sed dolores reprehenderit harum qui dolores consequatur quasi molestiae.
                Dolore quam maiores suscipit at alias et sint.
                Vel qui omnis et.
                Mollitia a aut repellendus et hic.", new DateTime(2021, 1, 1, 8, 45, 7, 663, DateTimeKind.Local).AddTicks(2551), "inventore", 38, 2, 4 },
                    { 11, new DateTime(2018, 12, 16, 23, 10, 32, 398, DateTimeKind.Local).AddTicks(8206), @"Neque hic commodi.
                Nam consequatur quo est minima.
                Saepe deserunt sint expedita at ut minima.
                Qui amet accusamus.
                Incidunt modi rerum consequatur ut provident.", new DateTime(2020, 4, 8, 18, 8, 37, 920, DateTimeKind.Local).AddTicks(6437), "quas", 11, 3, 1 },
                    { 12, new DateTime(2016, 7, 18, 15, 27, 18, 566, DateTimeKind.Local).AddTicks(9719), @"Asperiores commodi vitae numquam repudiandae vero pariatur.
                Tempora eos fugit expedita beatae rerum consequuntur ut.
                Sed quia placeat porro eos.
                At in nemo enim beatae.
                Vel modi eum quibusdam exercitationem.
                Repudiandae aut est enim perferendis.", new DateTime(2021, 8, 16, 12, 16, 1, 623, DateTimeKind.Local).AddTicks(3049), "error", 12, 3, 1 },
                    { 43, new DateTime(2019, 1, 4, 23, 26, 32, 922, DateTimeKind.Local).AddTicks(5294), @"Quia deserunt eos.
                Aut et debitis consequatur veniam placeat omnis quis minima.", new DateTime(2020, 5, 15, 11, 52, 29, 548, DateTimeKind.Local).AddTicks(7087), "ut", 43, 1, 3 },
                    { 48, new DateTime(2019, 6, 1, 11, 37, 26, 547, DateTimeKind.Local).AddTicks(4834), @"Consequuntur quo autem delectus doloremque iure doloribus ut qui quia.
                Quasi eligendi aliquid libero at voluptas molestias vero.", new DateTime(2022, 4, 10, 16, 19, 56, 51, DateTimeKind.Local).AddTicks(3730), "explicabo", 48, 5, 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "ProjectTasks",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
