using System;
using System.Collections.Generic;
using ProjectStructure.Common;

namespace ProjectStructure.DAL
{
    public class ProjectsContext
    {
        private static readonly List<ProjectDTO> _projects  = new List<ProjectDTO>();
        private static readonly List<ProjectTaskDTO> _projectTasks = new List<ProjectTaskDTO>();
        private static readonly List<UserDTO> _users = new List<UserDTO>(); 
        private static readonly List<TeamDTO> _teams = new List<TeamDTO>();
        private static readonly List<TaskStateDTO> _taskStates = new List<TaskStateDTO>();

        public List<ProjectDTO> Projects => _projects;
        public List<ProjectTaskDTO> ProjectTasks => _projectTasks;
        public List<UserDTO> Users => _users;
        public List<TeamDTO> Teams => _teams;
        public List<TaskStateDTO> TaskStates => _taskStates;

        
    }
}