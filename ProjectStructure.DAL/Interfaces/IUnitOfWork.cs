
using ProjectStructure.Common;
using ProjectStructure.DAL.Repositories;
namespace ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<ProjectDTO> ProjectRepository { get; }
        IRepository<UserDTO> UserRepository { get; }
        IRepository<TeamDTO> TeamRepository { get; }
        IRepository<TaskStateDTO> TaskStateRepository { get; }
        IRepository<ProjectTaskDTO> ProjectTaskRepository { get; }
    }
}