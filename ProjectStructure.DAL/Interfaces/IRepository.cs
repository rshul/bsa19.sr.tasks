using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IRepository<TEntity>
    {
         IEnumerable<TEntity> GetAll();
         TEntity GetById(int id);
         int Insert(TEntity entity);
         void Update(int id, TEntity entity);
         void Delete(int id);
    }
}