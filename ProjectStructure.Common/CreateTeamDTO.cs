﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Common
{
   public class CreateTeamDTO
    {
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
    }
}
