namespace ProjectStructure.Common
{
    public class DataEntityAccessException : System.Exception
    {
        public DataEntityAccessException(string message): base(message)
        {  }
    }
}