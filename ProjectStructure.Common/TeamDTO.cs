using System;
using ProjectStructure.Common.Interfaces;
namespace ProjectStructure.Common
{
    public class TeamDTO: IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
    }
}