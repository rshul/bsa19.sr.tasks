﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Common
{
    public class CreateProjectTaskDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Finished_At { get; set; }
        public int State { get; set; }
        public int Project_Id { get; set; }
        public int Performer_Id { get; set; }
    }
}
