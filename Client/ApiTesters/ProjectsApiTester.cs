﻿using Bogus;
using Client.Repositories;
using ProjectStructure.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client.ApiTesters
{
    public class ProjectsApiTester
    {
        private IRepo<ProjectDTO> repo;
        private List<ProjectDTO> projects;

        public ProjectsApiTester(IRepo<ProjectDTO> repo)
        {
            this.repo = repo;
        }
        public void GenerateProjects(int num)
        {
            var projectFaker = new Faker<ProjectDTO>()
                .RuleFor(o => o.Id, f => f.Random.Number(5, 40))
                .RuleFor(o => o.Author_Id, f => f.Random.Number(40))
                .RuleFor(o => o.Name, f => f.Lorem.Word())
                .RuleFor(o => o.Team_Id, f => f.Random.Number(20))
                .RuleFor(o => o.Description, f => f.Lorem.Sentence(9))
                .RuleFor(o => o.Created_At, f => f.Date.Past(2, new DateTime(2019, 1, 1)).ToUniversalTime())
                .RuleFor(o => o.Deadline, f => f.Date.Soon(60, DateTime.Now));

            projects = projectFaker.Generate(num);
        }
        public void  SendProjects()
        {
            foreach (var item in projects)
            {
                repo.Insert(item);
            }
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            return await repo.GetAll();
        }

        public async Task<ProjectDTO> GetOneProject(int id)
        {
            return await repo.GetById(id);
        }

        public async Task UpdateProject(ProjectDTO entity)
        {
            await repo.Update(entity);
        }

        public async Task DeleteProject(int id)
        {
            await repo.Delete(id);
        }

    }
}
