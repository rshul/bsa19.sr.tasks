﻿using ProjectStructure.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client.Repositories
{
    public class TeamsRepo : IRepo<TeamDTO>
    {
        private string path = "Teams";

        public TeamsRepo()
        {
            HttpService.SetBaseUrl(@"https://localhost:5001/api/");
        }
        public async Task Delete(int id)
        {
            await HttpService.DeleteObjectAsync(id, path);
        }

        public async Task<IEnumerable<TeamDTO>> GetAll()
        {
            return await HttpService.GetObjectsAsync<TeamDTO>(path);
        }

        public async Task<TeamDTO> GetById(int id)
        {
            return await HttpService.GetObjecttAsync<TeamDTO>(id, path);
        }

        public async Task Insert(TeamDTO entity)
        {
            await HttpService.CreateProductAsync<TeamDTO>(entity, path);
        }

        public async Task<bool> Update( TeamDTO entity)
        {
            return await HttpService.UpdateObjectAsync<TeamDTO>( entity, path);
        }
    }
}
