﻿using ProjectStructure.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client.Repositories
{
    public class TasksRepo : IRepo<ProjectTaskDTO>
    {
        private string path = "Tasks";

        public TasksRepo()
        {
            HttpService.SetBaseUrl(@"https://localhost:5001/api/");
        }
        public async Task Delete(int id)
        {
            await HttpService.DeleteObjectAsync(id, path);
        }

        public async Task<IEnumerable<ProjectTaskDTO>> GetAll()
        {
            return await HttpService.GetObjectsAsync<ProjectTaskDTO>(path);
        }

        public async Task<ProjectTaskDTO> GetById(int id)
        {
            return await HttpService.GetObjecttAsync<ProjectTaskDTO>(id, path);
        }

        public async Task Insert(ProjectTaskDTO entity)
        {
            await HttpService.CreateProductAsync<ProjectTaskDTO>(entity, path);
        }

        public async Task<bool> Update( ProjectTaskDTO entity)
        {
           return await HttpService.UpdateObjectAsync<ProjectTaskDTO>(entity, path);
        }
    }
}
