﻿using ProjectStructure.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client.Repositories
{
    public class TaskStatesRepo : IRepo<TaskStateDTO>
    {
        private string path = "TaskStates";

        public TaskStatesRepo()
        {
            HttpService.SetBaseUrl(@"https://localhost:5001/api/");
        }
        public async Task Delete(int id)
        {
            await HttpService.DeleteObjectAsync(id, path);
        }

        public async Task<IEnumerable<TaskStateDTO>> GetAll()
        {
            return await HttpService.GetObjectsAsync<TaskStateDTO>(path);
        }

        public async Task<TaskStateDTO> GetById(int id)
        {
            return await HttpService.GetObjecttAsync<TaskStateDTO>(id, path);
        }

        public async Task Insert(TaskStateDTO entity)
        {
            await HttpService.CreateProductAsync<TaskStateDTO>(entity, path);
        }

        public async Task<bool> Update( TaskStateDTO entity)
        {
            return await HttpService.UpdateObjectAsync<TaskStateDTO>( entity, path);
        }
    }
}
