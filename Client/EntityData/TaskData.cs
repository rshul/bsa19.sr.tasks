﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.EntityData
{
    public class TaskData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public string State { get; set; }
        public UserData Performer { get; set; }
    }
}
