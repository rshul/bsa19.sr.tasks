﻿using Client.Repositories;
using ProjectStructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Client
{
    public class TaskMarker
    {
        private readonly IRepo<ProjectTaskDTO> _trepo;
        private readonly Random _rnd = new Random();

        public TaskMarker(IRepo<ProjectTaskDTO> trepo)
        {
            _trepo = trepo;

        }

        public async Task<int> MarkRandomTaskWithDelay(int tDelay)
        {
            Console.WriteLine("Enter in Task");
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            Timer ti = new Timer(tDelay) { AutoReset = false };
            ti.Elapsed += async (s, e) =>
            {
                ti.Dispose();
                try
                {
                    var taskId = await MarkRandomTask();
                    tcs.SetResult(taskId);
                }
                catch (Exception exc)
                {
                    tcs.SetException(exc);
                }
            };

            ti.Start();
            return await tcs.Task;
        }


        public async Task<int> MarkRandomTask()
        {
            List<ProjectTaskDTO> tasks = await _trepo.GetAll() as List<ProjectTaskDTO>;
            int randomIndex = _rnd.Next(tasks.Count);
            ProjectTaskDTO task = tasks[randomIndex];
            task.State = 3;
            var success = await _trepo.Update(task);
            if (success)
            {
                return task.Id;
            }
            else
            {
                throw new Exception("Operation not successful");
            }
        }

    }
}
