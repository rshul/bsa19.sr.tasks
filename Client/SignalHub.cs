﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class SignalHub
    {
        private HubConnection _conn;

        public SignalHub(string path)
        {
            _conn = new HubConnectionBuilder()
                  .WithUrl(path)
                  .Build();

            _conn.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await _conn.StartAsync();
            };
          
        }

        public void SignalOn(Action<string> action)
        {
            _conn.On<string>("NewMessage", action);
        }

        public Task HubStart()
        {
           return _conn.StartAsync();
        }

        public Task HubStop()
        {
            return _conn.StopAsync();
        }


    }
}
