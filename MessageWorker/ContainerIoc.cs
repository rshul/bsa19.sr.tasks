﻿
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageWorker
{
    public class ContainerIoc
    {
        private static IServiceProvider serviceProvider;

         static ContainerIoc()
        {
            RegisterServices();
        }

        private static void RegisterServices()
        {
            var collection = new ServiceCollection();
            collection.AddScoped<IQueueService, QueueServiceWorkerBeta>();
            serviceProvider = collection.BuildServiceProvider();
            
        }

       static public T GetService<T>()
        {
            return serviceProvider.GetService<T>();
        }
        public static void DisposeServices()
        {
            if (serviceProvider == null)
            {
                return;
            }
            if (serviceProvider is IDisposable)
            {
                ((IDisposable) serviceProvider).Dispose();
            }
        }
    }
}
