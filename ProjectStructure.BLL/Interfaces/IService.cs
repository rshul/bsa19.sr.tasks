using ProjectStructure.Common.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IService<TEntity> where TEntity:class , IEntity
    {
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();
        int Create(TEntity entity);
        void Update(int id, TEntity entity);
        void Delete(int id);
    }
}