using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Common.Interfaces;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IServiceAsync<TEntity> : IChangesTrackableAsync where TEntity:class
    {
        Task<TEntity> GetAsync(int id);
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(int id);
    }
}