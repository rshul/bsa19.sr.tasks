using System.Collections.Generic;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class TaskStatesService : IService<TaskStateDTO>
    {
        private readonly IUnitOfWork _repo;
       
        public TaskStatesService(IUnitOfWork repo)
        {
            _repo = repo;
        }

        public int Create(TaskStateDTO entity)
        {
            return _repo.TaskStateRepository.Insert(entity);
        }

        public void Delete(int id)
        {
             _repo.TaskStateRepository.Delete(id);
        }

        public TaskStateDTO Get(int id)
        {
            return _repo.TaskStateRepository.GetById(id);
        }

        public IEnumerable<TaskStateDTO> GetAll()
        {
            return _repo.TaskStateRepository.GetAll();
        }

        public void Update(int id, TaskStateDTO entity)
        {
            _repo.TaskStateRepository.Update(id, entity);
        }
    }
}