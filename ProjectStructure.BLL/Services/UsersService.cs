using System.Collections.Generic;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class UsersService : IService<UserDTO>
    {
        private readonly IUnitOfWork _repo;

        public UsersService(IUnitOfWork repo)
        {
            _repo = repo;
        }

        public int Create(UserDTO entity)
        {
            return _repo.UserRepository.Insert(entity);
        }

        public void Delete(int id)
        {
             _repo.UserRepository.Delete(id);
        }

        public UserDTO Get(int id)
        {
            return _repo.UserRepository.GetById(id);
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return _repo.UserRepository.GetAll();
        }

        public void Update(int id, UserDTO entity)
        {
             _repo.UserRepository.Update(id, entity);
        }
    }
}