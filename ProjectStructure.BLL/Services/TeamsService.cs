using System.Collections.Generic;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class TeamsService : IService<TeamDTO>
    {
        private readonly IUnitOfWork _repo;

        public TeamsService(IUnitOfWork repo)
        {
            _repo = repo;
        }

        public int Create(TeamDTO entity)
        {
            return _repo.TeamRepository.Insert(entity);
        }

        public void Delete(int id)
        {
            _repo.TeamRepository.Delete(id);
        }

        public TeamDTO Get(int id)
        {
            return _repo.TeamRepository.GetById(id);
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            return _repo.TeamRepository.GetAll();
        }

        public void Update(int id, TeamDTO entity)
        {
            _repo.TeamRepository.Update(id, entity);
        }
    }
}