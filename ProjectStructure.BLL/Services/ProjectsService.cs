using System.Collections.Generic;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class ProjectsService : IService<ProjectDTO>
    {
        private readonly IUnitOfWork _repo;

        public ProjectsService(IUnitOfWork unitOfWork)
        {
            _repo = unitOfWork;
        }
        public int Create(ProjectDTO entity)
        {
           return _repo.ProjectRepository.Insert(entity);
        }

        public void Delete(int id)
        {
            _repo.ProjectRepository.Delete(id);
        }

        public ProjectDTO Get(int id)
        {
            return _repo.ProjectRepository.GetById(id);
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            return _repo.ProjectRepository.GetAll();
        }

        public void Update(int id, ProjectDTO entity)
        {
            _repo.ProjectRepository.Update(id, entity);
        }
    }
}