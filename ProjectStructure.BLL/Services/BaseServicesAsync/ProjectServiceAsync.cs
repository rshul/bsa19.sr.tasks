﻿using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class ProjectsServiceAsync : IServiceAsync<Project>
    {

        protected readonly IUnitOfWorkAsync _repo;

        public ProjectsServiceAsync(IUnitOfWorkAsync unitOfWork)
        {
            _repo = unitOfWork;
        }

        public async Task<Project> CreateAsync(Project entity)
        {
            return  await _repo.ProjectRepository.InsertAsync(entity);
        }

        public async Task DeleteAsync(int id)
        {
            await _repo.ProjectRepository.DeleteAsync(id);
        }

        public async Task<Project> GetAsync(int id)
        {
            return  await _repo.ProjectRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            return await _repo.ProjectRepository.GetAllAsync();
        }

        public async Task UpdateAsync(Project entity)
        {
            await  _repo.ProjectRepository.UpdateAsync(entity.Id, entity);
        }

        public async Task<int> SaveChangesAsync()
        {
            return  await _repo.SaveChangesAsync();
        }

        public async Task<bool> IsEntityPresentAsync(int id)
        {
            var result = await GetAsync(id);
            return result != null;
        }
    }
}
