﻿using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services.BaseServicesAsync
{
    public class TaskStateServiceAsync : IServiceAsync<TaskState>
    {
        protected readonly IUnitOfWorkAsync _repo;

        public TaskStateServiceAsync(IUnitOfWorkAsync unitOfWork)
        {
            _repo = unitOfWork;
        }

        public async Task<TaskState> CreateAsync(TaskState entity)
        {
            return await _repo.TaskStateRepository.InsertAsync(entity);
        }

        public async Task DeleteAsync(int id)
        {
            await _repo.TaskStateRepository.DeleteAsync(id);
        }

        public async Task<TaskState> GetAsync(int id)
        {
            return await _repo.TaskStateRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<TaskState>> GetAllAsync()
        {
            return await _repo.TaskStateRepository.GetAllAsync();
        }

        public async Task UpdateAsync(TaskState entity)
        {
            await _repo.TaskStateRepository.UpdateAsync(entity.Id, entity);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _repo.SaveChangesAsync();
        }

        public async Task<bool> IsEntityPresentAsync(int id)
        {
            var result = await GetAsync(id);
            return result != null;
        }
    }
}
