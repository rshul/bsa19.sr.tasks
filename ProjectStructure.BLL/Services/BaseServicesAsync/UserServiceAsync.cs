﻿using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services.BaseServicesAsync
{
    public class UserServiceAsync : IServiceAsync<User>
    {
        protected readonly IUnitOfWorkAsync _repo;

        public UserServiceAsync(IUnitOfWorkAsync unitOfWork)
        {
            _repo = unitOfWork;
        }

        public async Task<User> CreateAsync(User entity)
        {
            return await _repo.UserRepository.InsertAsync(entity);
        }

        public async Task DeleteAsync(int id)
        {
            await _repo.UserRepository.DeleteAsync(id);
        }

        public async Task<User> GetAsync(int id)
        {
            return await _repo.UserRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _repo.UserRepository.GetAllAsync();
        }

        public async Task UpdateAsync(User entity)
        {
            await _repo.UserRepository.UpdateAsync(entity.Id, entity);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _repo.SaveChangesAsync();
        }

        public async Task<bool> IsEntityPresentAsync(int id)
        {
            var result = await GetAsync(id);
            return result != null;
        }
    }
}
