﻿using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TaskServiceAscync : IServiceAsync<ProjectTask>
    {
        protected readonly IUnitOfWorkAsync _repo;

        public TaskServiceAscync(IUnitOfWorkAsync repo)
        {
            _repo = repo;
        }

        public async Task<ProjectTask> CreateAsync(ProjectTask entity)
        {
            return await _repo.ProjectTaskRepository.InsertAsync(entity);
        }

        public async Task DeleteAsync(int id)
        {
            await _repo.ProjectTaskRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<ProjectTask>> GetAllAsync()
        {
            return await _repo.ProjectTaskRepository.GetAllAsync();
        }

        public async Task<ProjectTask> GetAsync(int id)
        {
            return await _repo.ProjectTaskRepository.GetByIdAsync(id);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _repo.SaveChangesAsync();
        }

        public async Task UpdateAsync(ProjectTask entity)
        {
            await _repo.ProjectTaskRepository.UpdateAsync(entity.Id, entity);
        }

        public async Task<bool> IsEntityPresentAsync(int id)
        {
            var result = await GetAsync(id);
            return result != null;
        }
    }
}
