﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.BaseServicesAsync;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
   public class TeamServiceAsyncWithMapping : TeamServiceAsync, IServiceAsyncWithMapping<Team, TeamDTO, CreateTeamDTO>
    {
        private readonly IMapper _mapper;

        public TeamServiceAsyncWithMapping(IUnitOfWorkAsync unitOfWork, IMapper mapper) : base(unitOfWork)
        {
            _mapper = mapper;
        }

        public async Task<Team> CreateAsyncMapped(CreateTeamDTO entity)
        {
            var teamFromDTO = _mapper.Map<Team>(entity);
            return await CreateAsync(teamFromDTO);
        }

        public async Task<TeamDTO> GetAsyncMapped(int id)
        {
            var team = await GetAsync(id);
            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<IEnumerable<TeamDTO>> GetAllAsyncMapped()
        {
            var teams = await GetAllAsync();
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public async Task UpdateAsyncMapped(TeamDTO entity)
        {
            var canUpdate = await IsEntityPresentAsync(entity.Id);
            if (canUpdate)
            {
                var teamFromDTO = _mapper.Map<Team>(entity);
                await UpdateAsync(teamFromDTO);
            }
            else
            {
                throw new Exception("Not found entity");
            }
        }
    }
}
