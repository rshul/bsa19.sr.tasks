﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class ProjectServiceAsyncWithMapping : ProjectsServiceAsync, IServiceAsyncWithMapping<Project, ProjectDTO, CreatProjectDTO>
    {
        private readonly IMapper _mapper;

        public ProjectServiceAsyncWithMapping(IUnitOfWorkAsync unitOfWork, IMapper mapper) : base(unitOfWork)
        {
            _mapper = mapper;
        }

        public async Task<Project> CreateAsyncMapped(CreatProjectDTO entity)
        {
            var projectFromDTO = _mapper.Map<Project>(entity);
            return await CreateAsync(projectFromDTO);
        }


        public async Task<ProjectDTO> GetAsyncMapped(int id)
        {
            var project = await GetAsync(id);
            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllAsyncMapped()
        {
            var projects = await GetAllAsync();
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public async Task UpdateAsyncMapped(ProjectDTO entity)
        {
            var canUpdate = await IsEntityPresentAsync(entity.Id);
            if (canUpdate)
            {
                var projectFromDTO = _mapper.Map<Project>(entity);
                await UpdateAsync(projectFromDTO);
            }
            else
            {
                throw new Exception("Not found entity");
            }
        }


    }
}
