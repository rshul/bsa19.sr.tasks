using System.Collections.Generic;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class TasksService : IService<ProjectTaskDTO>
    {
        private readonly IUnitOfWork _repo;

        public TasksService(IUnitOfWork unitOfWork)
        {
            _repo = unitOfWork;
        }

        public int Create(ProjectTaskDTO entity)
        {
            return _repo.ProjectTaskRepository.Insert(entity);
        }

        public void Delete(int id)
        {
            _repo.ProjectTaskRepository.Delete(id);
        }

        public ProjectTaskDTO Get(int id)
        {
            return _repo.ProjectTaskRepository.GetById(id);
        }

        public IEnumerable<ProjectTaskDTO> GetAll()
        {
            return _repo.ProjectTaskRepository.GetAll();
        }

        public void Update(int id, ProjectTaskDTO entity)
        {
             _repo.ProjectTaskRepository.Update(id, entity);
        }
    }
}