﻿using AutoMapper;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class ProjectTaskProfile: Profile
    {
        public ProjectTaskProfile()
        {
            CreateMap<ProjectTaskDTO, ProjectTask>()
            .ForMember(e => e.PerformerId, cfg => cfg.MapFrom(dto => dto.Performer_Id))
            .ForMember(e => e.PojectId, cfg => cfg.MapFrom(dto => dto.Project_Id))
            .ForMember(e => e.CreatedAt, cfg => cfg.MapFrom(dto => dto.Created_At))
            .ForMember(e => e.FinishedAt, cfg => cfg.MapFrom(dto => dto.Finished_At))
            .ForMember(e => e.TaskStateId, cfg => cfg.MapFrom(dto => dto.State))
            .ForMember(e => e.State, cfg => cfg.Ignore())
            .ReverseMap()
            .ForPath(e => e.State, cfg => cfg.MapFrom(dto => dto.TaskStateId));

            CreateMap<CreateProjectTaskDTO, ProjectTask>()
            .ForMember(e => e.TaskStateId, cfg => cfg.MapFrom(dto => dto.State))
            .ForMember(e => e.PojectId, cfg => cfg.MapFrom(dto => dto.Project_Id))
            .ForMember(e => e.PerformerId, cfg => cfg.MapFrom(dto => dto.Performer_Id))
            .ForMember(e => e.CreatedAt, cfg => cfg.MapFrom(dto => dto.Created_At))
            .ForMember(e => e.FinishedAt, cfg => cfg.MapFrom(dto => dto.Finished_At))
            .ForMember(e => e.State, cfg => cfg.Ignore())
            .ReverseMap()
            .ForPath(e => e.State, cfg => cfg.MapFrom(dto => dto.TaskStateId));
        }
    }
}
