﻿using AutoMapper;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class TaskStateProfile : Profile
    {
        public TaskStateProfile()
        {
            CreateMap<TaskStateDTO, TaskState>()
                .ReverseMap();

            CreateMap<CreateTaskStateDTO, TaskState>();
               


        }
    }
}
