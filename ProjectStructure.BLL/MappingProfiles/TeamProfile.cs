﻿using AutoMapper;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class TeamProfile: Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>()
            .ForMember(e => e.CreatedAt, cfg => cfg.MapFrom(dto => dto.Created_At))
            .ReverseMap();

            CreateMap<CreateTeamDTO, Team>();
        }
    }
}
