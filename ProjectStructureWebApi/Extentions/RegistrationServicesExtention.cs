
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.BLL.MappingProfiles;
using System.Reflection;

namespace ProjectStructureWebApi.Extentions
{
    public static class RegistrationServicesExtention
    {
        public static void RegisterAutomapper(this IServiceCollection services){
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<ProjectTaskProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();

            }, typeof(ProjectProfile).GetTypeInfo().Assembly);

        }
    }
}