using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        //private IService<ProjectTaskDTO> _taskService;
        //private QueueServiceBeta _qs;
        private IServiceAsyncWithMapping<ProjectTask, ProjectTaskDTO, CreateProjectTaskDTO> _taskService;

        public TasksController(IServiceAsyncWithMapping<ProjectTask, ProjectTaskDTO, CreateProjectTaskDTO> taskService, QueueServiceBeta qs)
        {
            _taskService = taskService;
            //_qs = qs;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            var collection = await _taskService.GetAllAsyncMapped();
            return Ok(collection);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            ProjectTaskDTO foundEntity;
            try
            {
                foundEntity = await _taskService.GetAsyncMapped(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateProjectTaskDTO value)
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            var result = await _taskService.CreateAsyncMapped(value);
            await _taskService.SaveChangesAsync();
            return Ok(result);
        }

        // PUT api/values/5
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ProjectTaskDTO value)
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                await _taskService.UpdateAsyncMapped(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            await _taskService.SaveChangesAsync();
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            try
            {
                await _taskService.DeleteAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            await _taskService.SaveChangesAsync();
            return NoContent();
        }

    
    }
}