
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using ProjectStructure.BLL.Hubs;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        
        private IServiceAsyncWithMapping<Project,ProjectDTO,CreatProjectDTO> _projectService;
     
        // private QueueServiceBeta _qs;

        public ProjectsController(IServiceAsyncWithMapping<Project,ProjectDTO,CreatProjectDTO> projectService, IMapper mapper, IConfiguration config)
        {
            _projectService = projectService;
           // _qs =qs;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            var collection = await _projectService.GetAllAsyncMapped();
            return Ok(collection);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            //  var messageSource = (
            //     classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //     methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            // _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            ProjectDTO foundEntity;
            try
            {
                foundEntity = await _projectService.GetAsyncMapped(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CreatProjectDTO value)
        {
            //  var messageSource = (
            //     classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //     methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            // _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            var result  = await _projectService.CreateAsyncMapped(value);
            await _projectService.SaveChangesAsync();
            return Ok(result);
        }

        // PUT api/values/5
        [HttpPut]
        public async Task<IActionResult> Put(  [FromBody] ProjectDTO value)
        {
            //  var messageSource = (
            //     classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //     methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            // _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            { 
                await _projectService.UpdateAsyncMapped(value);  
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            await _projectService.SaveChangesAsync();
            return NoContent();

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            //  var messageSource = (
            //     classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //     methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            // _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                await _projectService.DeleteAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            await _projectService.SaveChangesAsync();
            return NoContent();

        }

    }
}