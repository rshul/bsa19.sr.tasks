using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;
using ProjectStructure.DAL.Entities;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private IServiceAsyncWithMapping<TaskState, TaskStateDTO, CreateTaskStateDTO> _taskStateService;
        //private QueueServiceBeta _qs;

        public TaskStatesController(IServiceAsyncWithMapping<TaskState, TaskStateDTO, CreateTaskStateDTO> taskStateService )
        {
            _taskStateService = taskStateService;
           
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            var collection = await _taskStateService.GetAllAsyncMapped();
            return Ok(collection);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            TaskStateDTO foundEntity;
            try
            {
                foundEntity = await _taskStateService.GetAsyncMapped(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateTaskStateDTO value)
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            var result = await _taskStateService.CreateAsyncMapped(value);
            await _taskStateService.SaveChangesAsync();
            return Ok(result);
        }

        // PUT api/values/5
        [HttpPut]
        public async Task<IActionResult> Put( [FromBody] TaskStateDTO value)
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                await _taskStateService.UpdateAsyncMapped(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            await _taskStateService.SaveChangesAsync();
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            //var messageSource = (
            //    classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
            //    methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            //_qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                await _taskStateService.DeleteAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            await _taskStateService.SaveChangesAsync();
            return NoContent();
        }






    }
}